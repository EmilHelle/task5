﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task5
{
    class ContactsSearcher
    {
        public List<Contact> Contacts { get; set; }
        public ContactsSearcher(List<Contact> contacts)
        {
            Contacts = contacts;
        }

        public void Search(string name)
        {
            List<Contact> resultList = Contacts.FindAll(contact =>
            (name == contact.FirstName || name == contact.LastName) ||
            (contact.FirstName.ToLowerInvariant().Contains(name) ||
            contact.LastName.ToLowerInvariant().Contains(name.ToLowerInvariant())));
            foreach (var contact in resultList)
            {
                Console.WriteLine(contact.ToString());
            }
        }

    }
}
