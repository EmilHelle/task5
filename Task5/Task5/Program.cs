﻿using System;
using System.Collections.Generic;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            //set up contact list
            List<Contact> contacts = new List<Contact>
            {
                new Contact("Sarah", "Jojo"),
                new Contact("Mark", "Toto"),
                new Contact("Tom", "Lo", 23098910),
                new Contact("Chris", "Hems"),
                new Contact("Tony", "Stark", 45239412)
            };

            //display contacts
            Console.WriteLine("Contacts:");
            foreach (Contact contact in contacts)
            {
                Console.WriteLine(contact.ToString());
            }
            //get user input
            Console.WriteLine();
            Console.WriteLine("Search for name");
            Console.Write("Enter Name :");

            string name = Console.ReadLine();
            //search contacts
            ContactsSearcher contactsSearcher = new ContactsSearcher(contacts);
            contactsSearcher.Search(name);

        }
    }
}
